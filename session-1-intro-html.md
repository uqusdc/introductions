Edited in [StackEdit](https://stackedit.io/)

# Intro to HTML

## Tools
### Editors
#### Desktop
Visual Studio Code (vscode): [https://code.visualstudio.com/](https://code.visualstudio.com/)

#### Web-Based Editors
vscode on the web: [https://vscode.dev/](https://vscode.dev/)

- Fully functional web-based version of vscode

CodePen: [https://codepen.io/](https://codepen.io/)

- Sandbox for protyping and experimentation
- Contains lots of good code examples that you can repurpose for your own projects


<br />




## What is HTML

- Standard Markup Language for Webpage
- Describes Page Structure
- Contains elements
- Defines the way what content is displayed
- Can do only limited styling

<br />






## HTML Elements

### Starting vs Ending Tags
Most html tags have both opening and closing tags.  
These are distinguished by the closing tag containing the forward slash as below.

	<title>
	      Captain Anonymous's Webpage
	</title>
    
    <h1>
        Captain Anonymous's Headline
    </h1>

    <p>
        Captain Anonymous's Paragraph
    </p>




### Self closing tags
A small number of tags are self closing. These don't have the closing tag, but are closed by the forward slash at the end of the statement as seen below.

	<img />
    <img src="https://source.unsplash.com/1024x768" />

<br />



## Simple Example Page

	<!DOCTYPE html>
	<html>
	  <head>
	    <title>
	      Captain Anonymous's Webpage
	    </title>
	  </head>
	  <body>
	    <div class="first-div">
	      <h1>Captain Anonymous's Webpage</h1>
	      <p>Captain Anonymous's Paragraphy copy</p>
	      <img src="https://source.unsplash.com/1024x768" />
	    </div>
	  </body>
	</html>

<br />




- - -
- - -




# CSS
- Cascading Style Sheets
- Define the style, visual representation of the content