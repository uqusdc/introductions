### This repository collects the notes covering coding training sessions given by RayW to the site and design teams

- - -

#### Session 1 - Intro to HTML
Session Date: 2022.02.18  
[Session 1 Notes](session-1-intro-html.md/)